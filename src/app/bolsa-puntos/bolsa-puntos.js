app.controller('bolsa-puntosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.bolsas = [];
        $scope.nuevo = {};
        $scope.consulta = {};

        var b1, b2 = false;

        blockUI();
        serviciosFactory.bolsaPuntos()
            .then(function(response) {
                    $scope.bolsas = response.data.bolsas;
                    console.log($scope.bolsas);
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });


        serviciosFactory.getClientes()
            .then(function(response) {
                    $scope.clientes = response.data.cliente;
                    console.log($scope.clientes);
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });

        $scope.verDetalles = function(cliente) {
            $localStorage.cliente = cliente;
            $state.go('app.detalles-cliente');
        };

        $scope.matchConcepto = function(id) {
            for (i = 0; i < $scope.conceptos.length; i++) {
                if ($scope.conceptos[i]._id == id) {
                    var completo = $scope.conceptos[i].descripcion;
                    return completo;
                }
                break;
            }
        };

        $scope.matchCliente = function(id) {
            for (i = 0; i < $scope.clientes.length; i++) {
                if ($scope.clientes[i]._id == id) {
                    var completo = $scope.clientes[i].nombre + ' ' + $scope.clientes[i].apellido;
                    return completo;
                }
                break;
            }
        };

        $scope.cargar = function() {
            var nuevo = {
                "monto": $scope.nuevo.monto,
                "idCliente": $scope.nuevo.cliente
            }

            $('#exampleModal').modal('hide');
            blockUI();
            serviciosFactory.cargarBolsa(nuevo)
                .then(function(response) {
                        toastr.success('Bolsa cargada con exito');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un al cargar la bolsa');
                        $state.reload();
                        unBlockUI();
                    });

        };

        $scope.consultar = function() {
            $scope.mostrarTabla = false;

            if (!$scope.consulta.cliente) {
                $scope.consulta.cliente = null;
            }

            if (!$scope.consulta.vencimiento) {
                $scope.consulta.vencimiento = null;
            }

            if (!$scope.consulta.estado) {
                $scope.consulta.estado = null;
            }

            console.log($scope.consulta.cliente, $scope.consulta.vencimiento, $scope.consulta.estado);
            blockUI();
            serviciosFactory.bolsasFiltro($scope.consulta.estado, $scope.consulta.vencimiento, $scope.consulta.cliente)
                .then(function(response) {
                        if (response.data.ok) {
                            $scope.filtrado = response.data.bolsas;
                            $scope.mostrarTabla = true;
                            console.log($scope.filtrado);
                        } else {
                            toastr.info('No se encuentran coincidencias');
                        }
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('La operación no pudo ser procesada. Inténtelo nuevamente.');
                        unBlockUI();
                    });
        }

    }
]);