var urlApi = '/sis-fidelizacion';

app.factory('serviciosFactory', ['$http', function($http) {
    return {
        login: function(parametros) {
            return $http({
                url: urlApi + '/usuario/autenticar',
                method: 'POST',
                data: parametros,
                withCredentials: true
            });
        },
        getClientes: function() {
            return $http({
                url: urlApi + '/cliente',
                method: 'GET',
                withCredentials: true
            });
        },
        crearCliente: function(data) {
            return $http({
                url: urlApi + '/cliente',
                method: 'POST',
                data: data,
                withCredentials: true
            });
        },
        getUsuarios: function() {
            return $http({
                url: urlApi + '/usuario',
                method: 'GET',
                withCredentials: true
            });
        },
        crearUsuario: function(data) {
            return $http({
                url: urlApi + '/usuario',
                method: 'POST',
                data: data,
                withCredentials: true
            });
        },
        eliminarUsuario: function(data) {
            return $http({
                url: urlApi + '/usuario/' + data,

                method: 'DELETE',
                withCredentials: true
            });
        },
        conceptoPuntos: function() {
            return $http({
                url: urlApi + '/puntos',
                method: 'GET',
                withCredentials: true
            });
        },
        eliminarConcepto: function(parametro) {
            return $http({
                url: urlApi + '/puntos/' + parametro,
                method: 'DELETE',
                withCredentials: true
            });
        },
        crearConcepto: function(parametro) {
            return $http({
                url: urlApi + '/puntos',
                method: 'POST',
                data: parametro,
                withCredentials: true
            });
        },
        getVencimientos: function() {
            return $http({
                url: urlApi + '/vencimiento',
                method: 'GET',
                withCredentials: true
            });
        },
        usoDePuntosFiltro: function(idConcepto, fechaDesde, fechaHasta, idCliente) {
            return $http({
                url: urlApi + '/detalle-uso-puntos/uso-de-puntos?idConcepto=' + idConcepto + '&fechaDesde=' + fechaDesde +
                    '&fechaHasta=' + fechaHasta + '&idCliente=' + idCliente,
                method: 'GET',
                withCredentials: true
            });
        },
        bolsaPuntos: function() {
            return $http({
                url: urlApi + '/bolsa-puntos',
                method: 'GET',
                withCredentials: true
            });
        },
        cargarBolsa: function(data) {
            return $http({
                url: urlApi + '/bolsa-puntos/cargar',
                method: 'POST',
                data: data,
                withCredentials: true
            });
        },
        bolsasFiltro: function(estado, vencimiento, idCliente) {
            return $http({
                url: urlApi + '/bolsa-de-puntos?estado=' + estado + '&vencimiento=' + vencimiento +
                    '&idCliente=' + idCliente,
                method: 'GET',
                withCredentials: true
            });
        },
        utilizarPuntos: function(parametro) {
            return $http({
                url: urlApi + '/detalle-uso-puntos/utilizar-puntos',
                method: 'POST',
                data: parametro,
                withCredentials: true
            });
        },
    };
}]);