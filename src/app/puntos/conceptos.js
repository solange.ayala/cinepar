app.controller('conceptosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.conceptos = [];
        $scope.nuevo = {};

        blockUI();
        serviciosFactory.conceptoPuntos()
            .then(function(response) {
                    $scope.conceptos = response.data.usoPuntos;
                    console.log($scope.conceptos);
                    unBlockUI();
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    unBlockUI();
                });

        $scope.crear = function() {
            var punto = {
                "descripcion": $scope.nuevo.descripcion,
                "puntosRequeridos": $scope.nuevo.requeridos
            }
            $('#exampleModal').modal('hide');
            blockUI();
            serviciosFactory.crearConcepto(punto)
                .then(function(response) {
                        toastr.success('Concepto creado con exito');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un al crear el concepto');
                        $state.reload();
                        unBlockUI();
                    });
        };

        $scope.eliminar = function(concepto) {
            blockUI();
            serviciosFactory.eliminarConcepto(concepto._id)
                .then(function(response) {
                        toastr.success('Eliminado exitosamente');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un error');
                        unBlockUI();
                    });
        };

    }
]);