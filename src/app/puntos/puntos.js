app.controller('puntosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.conceptos = [];
        $scope.nuevo = {};

        var b1, b2 = false;

        blockUI();
        serviciosFactory.conceptoPuntos()
            .then(function(response) {
                    $scope.conceptos = response.data.usoPuntos;
                    console.log($scope.conceptos);
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });


        serviciosFactory.getClientes()
            .then(function(response) {
                    $scope.clientes = response.data.cliente;
                    console.log($scope.clientes);
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });

        $scope.verDetalles = function(cliente) {
            $localStorage.cliente = cliente;
            $state.go('app.detalles-cliente');
        };

        $scope.matchConcepto = function(id) {
            for (i = 0; i < $scope.conceptos.length; i++) {
                if ($scope.conceptos[i]._id == id) {
                    var completo = $scope.conceptos[i].descripcion;
                    return completo;
                }
            }
        };

        $scope.matchCliente = function(id) {
            for (i = 0; i < $scope.clientes.length; i++) {
                if ($scope.clientes[i]._id == id) {
                    var completo = $scope.clientes[i].nombre + ' ' + $scope.clientes[i].apellido;
                    console.log(completo);
                    return completo;
                }
            }
        };

        $scope.consultar = function() {
            $scope.mostrarTabla = false;
            if ($scope.consulta.fechaDesde && $scope.consulta.fechaHasta) {
                var fechaDesdeFormateada = $scope.consulta.fechaDesde.getFullYear() + '/' + ($scope.consulta.fechaDesde.getMonth() + 1) +
                    '/' + $scope.consulta.fechaDesde.getDate();

                var fechaHastaFormateada = $scope.consulta.fechaHasta.getFullYear() + '/' + ($scope.consulta.fechaHasta.getMonth() + 1) +
                    '/' + $scope.consulta.fechaHasta.getDate();
            } else {
                var fechaDesdeFormateada = null;
                var fechaHastaFormateada = null;
            }

            if (!$scope.consulta.cliente) {
                $scope.consulta.cliente = null;
            }

            if (!$scope.consulta.concepto) {
                $scope.consulta.concepto = null;
            }

            console.log($scope.consulta.cliente, fechaDesdeFormateada, fechaHastaFormateada, $scope.consulta.concepto);
            blockUI();
            serviciosFactory.usoDePuntosFiltro($scope.consulta.concepto, fechaDesdeFormateada, fechaHastaFormateada, $scope.consulta.cliente)
                .then(function(response) {
                        $scope.filtrado = response.data.cabeceras;
                        $scope.mostrarTabla = true;
                        console.log($scope.filtrado);
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('La operación no pudo ser procesada. Inténtelo nuevamente.');
                        unBlockUI();
                    });
        }

    }
]);