app.controller('vencimientosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.venci = [];
        $scope.nuevo = {};

        blockUI();
        serviciosFactory.getVencimientos()
            .then(function(response) {
                    $scope.venci = response.data.vencimientos;
                    unBlockUI();
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    unBlockUI();
                });


    }
]);