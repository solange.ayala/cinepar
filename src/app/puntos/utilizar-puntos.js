app.controller('utilizar-puntosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.conceptos = [];
        $scope.nuevo = {};

        var b1, b2 = false;

        blockUI();
        serviciosFactory.conceptoPuntos()
            .then(function(response) {
                    $scope.conceptos = response.data.usoPuntos;
                    console.log($scope.conceptos);
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b1 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });


        serviciosFactory.getClientes()
            .then(function(response) {
                    $scope.clientes = response.data.cliente;
                    console.log($scope.clientes);
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    b2 = true;
                    if (b1 && b2) {
                        unBlockUI();
                    }
                });

        $scope.verDetalles = function(cliente) {
            $localStorage.cliente = cliente;
            $state.go('app.detalles-cliente');
        };

        $scope.matchConcepto = function(id) {
            for (i = 0; i < $scope.conceptos.length; i++) {
                if ($scope.conceptos[i]._id == id) {
                    var completo = $scope.conceptos[i].descripcion;
                    return completo;
                }
                break;
            }
        };

        $scope.matchCliente = function(id) {
            for (i = 0; i < $scope.clientes.length; i++) {
                if ($scope.clientes[i]._id == id) {
                    var completo = $scope.clientes[i].nombre + ' ' + $scope.clientes[i].apellido;
                    return completo;
                }
                break;
            }
        };

        $scope.utilizar = function() {
            $scope.mostrarTabla = false;

            var data = {
                "idCliente": $scope.utilizacion.cliente,
                "idConcepto": $scope.utilizacion.concepto
            }

            blockUI();
            serviciosFactory.utilizarPuntos(data)
                .then(function(response) {
                        console.log(response.data);
                        $('#exampleModal').modal('show');
                        $scope.mensaje = response.data.message;
                        //toastr.info(response.data.message);
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Operacion fallida');
                        unBlockUI();
                    });
        }

    }
]);