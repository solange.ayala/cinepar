app.controller('clientesCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.clientes = [];
        $scope.nuevo = {};

        blockUI();
        serviciosFactory.getClientes()
            .then(function(response) {
                    $scope.clientes = response.data.cliente;
                    unBlockUI();
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    unBlockUI();
                });

        $scope.verDetalles = function(cliente) {
            $localStorage.cliente = cliente;
            $state.go('app.detalles-cliente');
        };

        $scope.crear = function() {
            var cli = {
                "nombre": $scope.nuevo.nombre,
                "apellido": $scope.nuevo.apellido,
                "nroDocumento": $scope.nuevo.nroDocumento,
                "tipoDocumento": $scope.nuevo.tipoDocumento,
                "nacionalidad": $scope.nuevo.nacionalidad,
                "email": $scope.nuevo.email,
                "telefono": $scope.nuevo.telefono,
                "fechaNacimiento": $scope.nuevo.fechaNacimiento
            }

            blockUI();
            serviciosFactory.crearCliente(cli)
                .then(function(response) {
                        toastr.success('Cliente creado con exito');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un al crear el cliente');
                        $state.reload();
                        unBlockUI();
                    });
        };

    }
]);