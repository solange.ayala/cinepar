app.controller('crear-clienteCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.clientes = [];

        blockUI();
        serviciosFactory.getClientes()
            .then(function(response) {
                    $scope.clientes = response.data.cliente;
                    unBlockUI();
                },
                function(response) {
                    toastr.error('Ocurrio un error con las sucursales');
                    unBlockUI();
                });

        $scope.verDetalles = function(cliente) {
            $localStorage.cliente = cliente;
            $state.go('app.detalles-cliente');
        };

    }
]);