app.controller('usuariosCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state', '$filter', 'NgTableParams',
    function($scope, serviciosFactory, $localStorage, $state, $filter, NgTableParams) {

        $scope.usuarios = [];
        $scope.nuevo = {};

        blockUI();
        serviciosFactory.getUsuarios()
            .then(function(response) {
                    $scope.usuarios = response.data.usuarios;
                    unBlockUI();
                },
                function(response) {
                    toastr.error('Ocurrio un error');
                    unBlockUI();
                });

        $scope.eliminar = function(usuario) {
            console.log(usuario._id);
            blockUI();
            serviciosFactory.eliminarUsuario(usuario._id)
                .then(function(response) {
                        toastr.success('Eliminado exitosamente');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un error');
                        unBlockUI();
                    });
        };

        $scope.crear = function() {
            var usu = {
                "nombre": $scope.nuevo.nombre,
                "email": $scope.nuevo.email,
                "password": $scope.nuevo.password
            }
            $('#exampleModal').modal('hide');
            blockUI();
            serviciosFactory.crearUsuario(usu)
                .then(function(response) {
                        toastr.success('Usuario creado con exito');
                        $state.reload();
                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Ocurrio un al crear el usuario');
                        $state.reload();
                        unBlockUI();
                    });
        };

    }
]);