app.controller('menuCtrl', ['$scope', 'serviciosFactory', '$localStorage', '$state',
    function($scope, serviciosFactory, $localStorage, $state) {

        $scope.usuario = $localStorage.socio;

        $scope.logout = function() {
            localStorage.clear();
            $state.go('login');
        };
        $('.nav-item > a').click(function() {
            $('li').removeClass('active');
            $(this).parent().addClass('active');
        });

    }
]);