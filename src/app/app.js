var app = angular.module('ng-dentalcare', ['ui.router', 'ngStorage', 'ngTable'])
    .config(function($stateProvider, $urlRouterProvider, $qProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                cache: false,
                templateUrl: 'app/login/login.html',
                controller: 'loginCtrl',
                module: 'public'
            })
            .state('app', {
                url: '/app',
                cache: false,
                abstract: true,
                templateUrl: 'app/menu/menu.html',
                controller: 'menuCtrl',
                module: 'private'
            })
            .state('app.inicio', {
                url: '/inicio',
                templateUrl: 'app/main/main.html',
                controller: 'mainCtrl',
                module: 'private'
            })

        /*---------CLIENTE--------*/

        .state('app.clientes', {
                url: '/clientes',
                templateUrl: 'app/clientes/clientes.html',
                controller: 'clientesCtrl',
                module: 'private'
            })
            .state('app.detalles-cliente', {
                url: '/detalles-cliente',
                templateUrl: 'app/clientes/detalles-cliente.html',
                controller: 'detalles-clienteCtrl',
                module: 'private'
            })
            .state('app.crear-cliente', {
                url: '/crear-cliente',
                templateUrl: 'app/clientes/crear-cliente.html',
                controller: 'crear-clienteCtrl',
                module: 'private'
            })

        /*---------CLIENTE--------*/

        /*---------USUARIO--------*/

        .state('app.usuarios', {
            url: '/usuarios',
            templateUrl: 'app/usuarios/usuarios.html',
            controller: 'usuariosCtrl',
            module: 'private'
        })

        /*---------USUARIO--------*/

        .state('app.realizar-reserva', {
            url: '/realizar-reserva',
            templateUrl: 'app/realizar-reserva/realizar-reserva.html',
            controller: 'realizaReservaCtrl',
            module: 'private'
        })



        /*---------PUNTOS--------*/
        .state('app.puntos', {
                url: '/puntos',
                templateUrl: 'app/puntos/puntos.html',
                controller: 'puntosCtrl',
                module: 'private'
            })
            .state('app.vencimiento-puntos', {
                url: '/vencimiento-puntos',
                templateUrl: 'app/puntos/vencimientos.html',
                controller: 'vencimientosCtrl',
                module: 'private'
            })
            /*---------PUNTOS--------*/

        /*---------PUNTOS--------*/
        .state('app.bolsa-puntos', {
            url: '/bolsa-puntos',
            templateUrl: 'app/bolsa-puntos/bolsa-puntos.html',
            controller: 'bolsa-puntosCtrl',
            module: 'private'
        })

        .state('app.utilizar-puntos', {
            url: '/utilizar-puntos',
            templateUrl: 'app/puntos/utilizar-puntos.html',
            controller: 'utilizar-puntosCtrl',
            module: 'private'
        })

        .state('app.conceptos-puntos', {
                url: '/conceptos-puntos',
                templateUrl: 'app/puntos/conceptos.html',
                controller: 'conceptosCtrl',
                module: 'private'
            })
            /*---------PUNTOS--------*/


        $urlRouterProvider.otherwise('/login');
        $qProvider.errorOnUnhandledRejections(false);

    });