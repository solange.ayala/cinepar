app.controller('loginCtrl', ['$scope', 'serviciosFactory',
    '$localStorage', '$state',
    function($scope, serviciosFactory, $localStorage, $state) {

        $scope.datos = {};
        $scope.datos.documento = "";
        $scope.datos.pass = "";
        $scope.usuario = [];

        $scope.login = function() {


            var parametros = {
                "email": $scope.datos.documento,
                "password": $scope.datos.pass
            };

            blockUI();
            serviciosFactory.login(parametros)
                .then(function(response) {
                        $('#login').removeClass('bounceIn');
                        $('#login').addClass('bounceOut');
                        setTimeout(function() {
                            $state.go('app.inicio');
                        }, 500);

                        unBlockUI();
                    },
                    function(response) {
                        toastr.error('Credenciales invalidas');
                        unBlockUI();
                    });
        };
    }
]);