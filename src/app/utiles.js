function blockUI() {
    $.blockUI.defaults.css = {};
    $.blockUI({
        message: '<div class="spinner">'
    });
}

function unBlockUI() {
    $.unblockUI();
}

function contador(solicitudes) {
    angular.forEach(solicitudes, function(value, key) {
        if (value.estado == 'APROBADO') {
            $localStorage.cAprobadas += 1;
        } else if (value.estado == 'RECHAZADO') {
            $localStorage.cRechazadas += 1;
        } else if (value.estado == 'PENDIENTE') {
            $localStorage.cPendientes += 1;
        }
    });
    console.log('ap', $localStorage.cAprobadas);
    console.log('rec', $localStorage.cRechazadas);
    console.log('pen', $localStorage.cPendientes);
}

function formatoFecha(input) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(input);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}