var gulp = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var inject = require('gulp-inject');
var series = require('stream-series');
var stripDebugJs = require('gulp-strip-debug');
var stripDebugCss = require('gulp-strip-css-comments');
var ngAnnotate = require('gulp-ng-annotate');
var imagemin = require('gulp-imagemin');
var templateCache = require('gulp-angular-templatecache');
var watch = require('gulp-watch');
var livereload = require('gulp-livereload');
connect = require('gulp-connect-multi')();
var proxy = require('http-proxy-middleware');
var plumber = require('gulp-plumber');

var paths = {
    index: {
        input: 'src/index.html',
        output: 'dist'
    },
    css: {
        input: [
            'src/assets/css/style.css'
        ],
        output: 'dist/main/css'
    },
    vendorCss: {
        input: [
            "node_modules/@fortawesome/fontawesome-free/css/all.min.css",
            "node_modules/toastr/build/toastr.css",
            "node_modules/bootstrap3/dist/css/bootstrap.css",
            "node_modules/animate.css/animate.min.css",
            "node_modules/angularjs-datepicker/src/css/angular-datepicker.css"
        ],
        output: 'dist/main/css'
    },
    js: {
        input: [
            'src/app/**/*.js'
        ],
        output: 'dist/main/js'
    },
    vendorJs: {
        input: [
            "node_modules/angular/angular.min.js",
            "node_modules/jquery/dist/jquery.min.js",
            //"node_modules/dist/js/bootstrap.min.js",
            "node_modules/bootstrap3/dist/js/bootstrap.js",
            "node_modules/angular-ui-router/release/angular-ui-router.min.js",
            "node_modules/ngstorage/ngStorage.min.js",
            "node_modules/ng-table/bundles/ng-table.min.js",
            "node_modules/angular-i18n/angular-locale_es-py.js",
            "node_modules/blockui/jquery.blockui.min.js",
            "node_modules/toastr/toastr.js",
            "node_modules/angularjs-datepicker/dist/angular-datepicker.js",
        ],
        output: 'dist/main/js'
    }
};


var htmlSources = ['src/app/**/*.html'];
var jsSources = ['src/app/**/*.js'];
var cssSources = ['src/css/**/*.css'];
var allSources = htmlSources.concat(cssSources).concat(jsSources);

gulp.task('jshint', function() {
    return gulp.src(paths.js.input)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

/**BUILD JS */
gulp.task('build-app-js', function() {
    return gulp.src(paths.js.input)
        .pipe(plumber())
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init())
        .pipe(concat('bundle-app.js'))
        .pipe(uglify())
        .on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.js.output));
});

gulp.task('build-vendor-js', function() {
    return gulp.src(paths.vendorJs.input)
        .pipe(concat('bundle-vendor.js'))
        .pipe(stripDebugJs())
        .pipe(uglify())
        .pipe(gulp.dest(paths.vendorJs.output));
});

gulp.task('build-js', ['build-vendor-js', 'build-app-js']);

/**BUILD CSS */
gulp.task('build-app-css', function() {
    return gulp.src(paths.css.input)
        .pipe(sourcemaps.init())
        .pipe(concat('bundle-app.css'))
        .pipe(stripDebugCss())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.css.output));
});

gulp.task('build-vendor-css', function() {
    return gulp.src(paths.vendorCss.input)
        .pipe(concat('bundle-vendor.css'))
        .pipe(stripDebugCss())
        .pipe(cleanCSS())
        .pipe(gulp.dest(paths.vendorCss.output));
});

gulp.task('build-css', ['build-vendor-css', 'build-app-css']);

gulp.task('src-inject', ['build-js', 'build-css'], function() {
    var target = gulp.src(paths.index.input);

    var appJsSource = gulp.src(['dist/main/js/bundle-app.js']);
    var vendorJsSource = gulp.src(['dist/main/js/bundle-vendor.js']);
    var templateCacheJsSource = gulp.src(['dist/main/js/templates.js']);

    var appCssSource = gulp.src(['dist/main/css/bundle-app.css']);
    var vendorCssSource = gulp.src(['dist/main/css/bundle-vendor.css']);

    return target
        .pipe(inject(
                series(vendorCssSource, appCssSource, vendorJsSource, appJsSource, templateCacheJsSource), { ignorePath: 'dist', addRootSlash: false })

        )
        .pipe(gulp.dest('dist'));
});

gulp.task('templateCache', function() {
    return gulp.src('src/app/**/*.html')
        .pipe(templateCache({
            'filename': 'templates.js',
            'root': 'app/',
            'module': 'ng-dentalcare'
        })).pipe(uglify())
        .pipe(gulp.dest('dist/main/js'));
});

gulp.task('src-copy-img', function() {
    return gulp.src(['src/assets/images/**.*'])
        .pipe(imagemin())
        .pipe(gulp.dest('dist/assets/images'));
});


// los webfonts del error del console 
gulp.task('src-copy-fonts-scss-fa', function() {
    return gulp.src(['node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf',
            'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2',
            'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff'
        ])
        .pipe(gulp.dest('dist/main/webfonts'));
});


gulp.task('src-copy-fonts', [
    'src-copy-fonts-scss-fa'
]);

gulp.task('livereload', ['build-app-js', 'templateCache'], function() {
    gulp.src(allSources)
        .pipe(connect.reload());
})

gulp.task('watch', function() {
    gulp.watch('src/app/**/*.js', ['build-app-js', 'jshint']);
    gulp.watch('src/app/**/*.html', ['templateCache']);
    gulp.watch('src/assets/css/*.css', ['build-app-css']);
    gulp.watch(allSources, ['livereload']);
});

gulp.task('connect', ['src-inject'], connect.server({
    livereload: true,
    root: ['dist'],
    port: 8000,
    open: {
        browser: 'chrome' // if not working OS X browser: 'Google Chrome'
    },
    middleware: function(connect, opt) {
        return [
            proxy('/sis-fidelizacion', {
                //target: 'http://192.168.13.19:3000',
                target: 'https://sis-fidelizacion.herokuapp.com',
                changeOrigin: true
            })
        ];
    }
}));

gulp.task('default', function() {
    gulp.start('jshint', 'build-js', 'build-css', 'src-inject', 'templateCache', 'src-copy-img', 'src-copy-fonts', 'watch', 'connect');
});